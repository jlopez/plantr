

/**
 * Draw quatrat 
 **/
Shiny.addCustomMessageHandler("drawPointQuadrat",

  function(data) {
    canvas.pointsQuadrat = [];
    canvas.segments = [];
    
    var minX = 0;
    var minY = 0;
    
    for(let i = 0; i < data.length; i++){
       var p = data[i];
       var x = p.x;
       var y = p.y;
       
       if(x < minX) {
         minX = x;
       }
       
       if(y < minY) {
         minY = y;
       }
      
    }
    
    for(let i = 0; i < data.length; i++){
       var p = data[i];
       var x = p.x;
       var y = p.y;
       var name = p.name;
       var point = new Point2D(x, y, name, -1);
       canvas.pointsQuadrat.push(point);
    }
    
    var quadrat = document.getElementById("quadrat-selectized").parentElement.childNodes[0].innerHTML;
    
  
    if(quadrat == "3") {
      canvas.segments.push(new Segment2D(canvas.pointsQuadrat[0], canvas.pointsQuadrat[1]));
      canvas.segments.push(new Segment2D(canvas.pointsQuadrat[1], canvas.pointsQuadrat[2]));
      canvas.segments.push(new Segment2D(canvas.pointsQuadrat[2], canvas.pointsQuadrat[3]));
      canvas.segments.push(new Segment2D(canvas.pointsQuadrat[3], canvas.pointsQuadrat[4]));
      canvas.segments.push(new Segment2D(canvas.pointsQuadrat[4], canvas.pointsQuadrat[5]));
      canvas.segments.push(new Segment2D(canvas.pointsQuadrat[5], canvas.pointsQuadrat[0]));
    }
    else if(quadrat == "9") {
      canvas.segments.push(new Segment2D(canvas.pointsQuadrat[0], canvas.pointsQuadrat[1]));
    }
    else if(quadrat == "4") {
      canvas.segments.push(new Segment2D(canvas.pointsQuadrat[0], canvas.pointsQuadrat[1]));
      canvas.segments.push(new Segment2D(canvas.pointsQuadrat[1], canvas.pointsQuadrat[2]));
      canvas.segments.push(new Segment2D(canvas.pointsQuadrat[2], canvas.pointsQuadrat[3]));
      canvas.segments.push(new Segment2D(canvas.pointsQuadrat[3], canvas.pointsQuadrat[4]));
      canvas.segments.push(new Segment2D(canvas.pointsQuadrat[4], canvas.pointsQuadrat[0]));
    }
    else if(quadrat == "17") {
      canvas.segments.push(new Segment2D(canvas.pointsQuadrat[0], canvas.pointsQuadrat[1]));
      canvas.segments.push(new Segment2D(canvas.pointsQuadrat[1], canvas.pointsQuadrat[2]));
      canvas.segments.push(new Segment2D(canvas.pointsQuadrat[2], canvas.pointsQuadrat[0]));
    }
    else if(quadrat == "27") {
      canvas.segments.push(new Segment2D(canvas.pointsQuadrat[0], canvas.pointsQuadrat[1]));
      canvas.segments.push(new Segment2D(canvas.pointsQuadrat[1], canvas.pointsQuadrat[2]));
      canvas.segments.push(new Segment2D(canvas.pointsQuadrat[2], canvas.pointsQuadrat[0]));
    }
    else if(quadrat == "38") {
      canvas.segments.push(new Segment2D(canvas.pointsQuadrat[0], canvas.pointsQuadrat[1]));
      canvas.segments.push(new Segment2D(canvas.pointsQuadrat[1], canvas.pointsQuadrat[2]));
      canvas.segments.push(new Segment2D(canvas.pointsQuadrat[2], canvas.pointsQuadrat[0]));
    }
    else if(quadrat == "39") {
      canvas.segments.push(new Segment2D(canvas.pointsQuadrat[0], canvas.pointsQuadrat[1]));
      canvas.segments.push(new Segment2D(canvas.pointsQuadrat[1], canvas.pointsQuadrat[2]));
      canvas.segments.push(new Segment2D(canvas.pointsQuadrat[2], canvas.pointsQuadrat[3]));
      canvas.segments.push(new Segment2D(canvas.pointsQuadrat[3], canvas.pointsQuadrat[0]));
    }
    else {
      canvas.segments.push(new Segment2D(canvas.pointsQuadrat[0], canvas.pointsQuadrat[1]));
      canvas.segments.push(new Segment2D(canvas.pointsQuadrat[1], canvas.pointsQuadrat[2]));
      canvas.segments.push(new Segment2D(canvas.pointsQuadrat[2], canvas.pointsQuadrat[3]));
      canvas.segments.push(new Segment2D(canvas.pointsQuadrat[3], canvas.pointsQuadrat[0]));
    }

    minX *= -1;
    minY *= -1;

    ctx.translate(canvas.lastXT, canvas.lastYT);
    ctx.translate(minX+30,minY+30);
    canvas.lastXT = (-1)*(minX+30);
    canvas.lastYT = (-1)*(minY+30);
    canvas.redraw();
    
  }
);

// =================================================================================================================

/**
 *  Clean plot
 */
Shiny.addCustomMessageHandler("cleanPlot",
  function(data) {
    
    canvas.points = [];
    canvas.pointsQuadrat = [];
    canvas.segments = [];
    canvas.redraw();
});

// =================================================================================================================

/**
 *Draw plant points on plot
 */
Shiny.addCustomMessageHandler("drawPoint",
  function(data) {
  
    canvas.points = [];
    
    console.log(data);
    
    for(let i = 0; i < data.x.length; i++){
       var x = data.x[i];
       var y = data.y[i];
       var name = data.name[i];
       var value = data.value[i];
       var point = new Point2D(x, y, name, value);
       canvas.points.push(point);
    }

    canvas.redraw();
});
