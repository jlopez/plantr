#@author jimmy.lopez@univ-montp2.fr

#Rscript -e 'shiny::runApp(".", host="127.0.0.1", port=4123)' &

library(shiny)
library(shinydashboard)
library(shinyjs)
library(RSQLite)
library(pool)
library(DT)
library(dplyr)
library(shinycssloaders)
library(jsonlite)
library(grid)
library(gridExtra)
library(pdftools)
library(ggplot2)


source("./R/db_query.R")
source("./R/helper_functions.R")
source("./R/menugauche.R", local = T)
source("./pages/pages_def_home.R", local = T)

Sys.setenv("plotly_username"="jlopez23")
Sys.setenv("plotly_api_key"="Div2THicOgL02haI20AD")

options(encoding = 'UTF-8')

#style <- tags$style(HTML(readLines("www/added_styles.css")) )
UI <- dashboardPage(
  skin = "green",
  dashboardHeader(title = "PlantR"),
  dashboardSidebar(MenuGauche),
  dashboardBody(
    
    shinyjs::useShinyjs(),
    tags$head(tags$link(rel = "stylesheet", type = "text/css", href = "bootstrap.min.readable.css")) ,
    #tags$head(tags$script(src = "http://www.ecare-community.co.uk/include/modules_js/plugins/jquery-knob/js/jquery.knob.js")),
    tags$head(tags$script(src = "message-handler.js")),
    #tags$head(tags$link(rel="shortcut icon", href="fav.ico")),
    #tags$head(style),
    
    tabItems(
      tabItem(tabName = "Home",         tabHome)
    )
  )
)

server <- function( input, output, session) {
  
  AUZILS <<- read.csv2("./data/Auzils.csv")
  CRUZADE <<- read.csv2("./data/Cruzade.csv")
  ENFERET1 <<- read.csv2("./data/Enferet1.csv")
  ENFERET2 <<- read.csv2("./data/Enferet2.csv")
  INTRO <<- read.csv2("./data/intro.csv")
  VIGIE <<- INTRO %>% filter(pop == "vi")
  INTRO  <<- INTRO%>% filter(pop == "ch")
  PEYRAL <<- read.csv2("./data/Peyral.csv")
  PORTES <<- read.csv2("./data/Portes.csv")
  
  con <- file("./data/pointes.txt", open = "r")
  POINTES_TITLE <<- readLines(con)
  close(con)
  
  source("./server/opt_home.R", local=TRUE)
}

shinyApp(ui = UI, server = server)