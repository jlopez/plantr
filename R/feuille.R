# ce petit programme permet de construire les feuilles de terrains
## en ne gardant que les lignes non vides pour les 4 dernières colonnes
### si on veut plus il suffit de modifier les lignes avec la variable conserver
#### dans la serie des programmes c est le second a faire

##############Mesure  en B de plante 118 danq Q4 auzils (>300 => transformé en 300)
##############Mesure  en B de plante 386 danq Q1 E1 (>300 => transformé en 300)


rm(list=ls())

monfichier=function(init,sortie)
{
debut=grep("J94", colnames(init)) #permet de reperer la premiere ligne de donnees J94
fin=ncol(init)-4 #permet de garder les 4 dernières colonnes

sousinit=init[,-c(debut:fin)] 

write.table(t(colnames(sousinit)),sortie, sep = "\t",quote=FALSE, row.names=F, col.names=F,append=F)
 
lig=nrow(sousinit)
nbcol=ncol(sousinit)

for (i in 1:lig)
 {conserver=0
  conserver=conserver+(ifelse(sousinit[i,nbcol]!=0,1,0))
  conserver=conserver+(ifelse(sousinit[i,nbcol-1]!=0,1,0))
  conserver=conserver+(ifelse(sousinit[i,nbcol-2]!=0,1,0))
  conserver=conserver+(ifelse(sousinit[i,nbcol-3]!=0,1,0))
 
  if (conserver>0)
  write.table(sousinit[i,],sortie,
  sep = "\t", na= "NA", dec = ",", quote=FALSE, col.names=F, row.names=F, append=TRUE)
  
 } 

}

entree=read.csv("Enferet1.csv",header=T, dec=',');exit=c("E1out.csv");monfichier(entree,exit)
entree=read.csv("Enferet2.csv",header=T, dec=',');exit=c("E2out.csv");monfichier(entree,exit)
entree=read.csv("Auzils.csv",header=T, dec=',');exit=c("auzilsout.csv");monfichier(entree,exit)
entree=read.csv("Cruzade.csv",header=T, dec=',');exit=c("cruzadesout.csv");monfichier(entree,exit)
entree=read.csv("Peyral.csv",header=T, dec=',');exit=c("peyralsout.csv");monfichier(entree,exit)
entree=read.csv("Portes.csv",header=T, dec=',');exit=c("portessout.csv");monfichier(entree,exit)
