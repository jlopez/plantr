MenuGauche = sidebarMenu(id = "sidebarmenu",
                         menuItem("Home", tabName = "Home",  icon = icon("home", lib="font-awesome")),
                         
                         br(),
                         
                         selectInput("pointes", "Pop:",
                                     POPNAMES),
                         
                         selectInput("quadrat", "Quadrat:", c()),
                         
                         #selectInput("dateAnalyse", "Date:", c()),
                         
                         actionButton("drawAnalyse", "Draw", class="btn btn-primary"),
                         
                         actionButton("redrawAnalyse2", "Redraw with corrections", class="btn btn-primary"),
                         
                         br(), br(),
                         
                         #downloadButton("downloadData", label = "Download Pop CSV", class = "btn btn-info"),
                         br(),br(),
                         downloadButton("downloadPDF", label = "Download PDF", class = "btn btn-info"),
                         
                         br(), br(), br(),
                         
                         menuItem("Team", icon = icon("book", lib="font-awesome"),
                                  menuItem("Eric Imbert",  href = "http://www.isem.univ-montp2.fr/fr/personnel/equipes/metapopulations/imbert-eric.index/", newtab = TRUE,     icon = shiny::icon("male"), selected = NULL  ),
                                  menuItem("Khalid Belkhir",  href = "http://www.isem.univ-montp2.fr/recherche/les-plate-formes/bioinformatique-labex/personnel/belkhir-khalid/", newtab = TRUE,     icon = shiny::icon("male"), selected = NULL  ),
                                  menuItem("Jimmy Lopez",  href = "http://www.isem.univ-montp2.fr/recherche/les-plate-formes/bioinformatique-labex/personnel/lopez-jimmy/", newtab = TRUE,   icon = shiny::icon("male"), selected = NULL  )
                         )
)