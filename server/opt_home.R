#' Event when pointes is update
observe({
  pointe <- input$pointes
  
  quadrat <- getQuadratByPop(pointe)
  
  updateSelectInput(session, "quadrat",
                    label = "Quadrat :",
                    choices = quadrat,
                    selected = quadrat[1]
  )
})

# ================================================================================================================


#' Event when download PDF button is push
output$downloadPDF <- downloadHandler(
  filename = function() {
    paste(getFullPopName(input$pointes),"_",input$quadrat, '.pdf', sep='')
  },
  content = function(con) {
    p <- LAST_GGPLOT
    
    # Save plot to pdf 1
    ggsave(paste0(con, ".1.pdf"), width = 30, height = 30)
    
    downDATA <- LASTDATAVALUE
    
    maxPID <- 0
    
    if(nrow(downDATA) > 0) {
    
    maxPID <- max(downDATA$PID)
    
    downDATA$PID <- as.character(downDATA$PID)
    downDATA$POP <- as.character(downDATA$POP)
    downDATA$Q <- as.character(downDATA$Q)
    downDATA$A <- as.character(downDATA$A)
    downDATA$B <- as.character(downDATA$B)
    downDATA$C <- as.character(downDATA$C)
    downDATA$D <- as.character(downDATA$D)
    downDATA$E <- as.character(downDATA$E)
    downDATA$F <- as.character(downDATA$F)
    downDATA$G <- as.character(downDATA$G)
    
    pop <- downDATA[1,]$POP
    q <- downDATA[1,]$Q
    
    # Create 10 new empty plant slot
    for(i in (maxPID+1):(maxPID+11)) {
      data <- data.frame(PID = i, POP = pop, Q = q, A = "", B = "", C = "", D = "", E = "", F = "", G = "", value = "", rem = "", stringsAsFactors = FALSE)
      downDATA <- rbind( downDATA, data)
    }
    
    colnames(downDATA) <- paste0("    ", colnames(LASTDATAVALUE), "    ")
    colnames(downDATA)[12] <- paste0("            ", colnames(downDATA)[12], "            ")
    tg = gridExtra::tableGrob(downDATA, rows = NULL )
    h = grid::convertHeight(sum(tg$heights), "in", TRUE)
    
    #Save table to pdf 2
    ggplot2::ggsave(paste0(con, ".2.pdf"), tg, width=20, height = h)
  
    pdf_combine(c(paste0(con, ".1.pdf"), paste0(con, ".2.pdf")), output = con)
    
    }
    else {
      
      maxPID <- 0
      
      pop <- input$pointes
      q <- input$quadrat
      
      # Create 10 new empty plant slot
      for(i in (maxPID+1):(maxPID+11)) {
        data <- data.frame(PID = i, POP = pop, Q = q, A = "", B = "", C = "", D = "", E = "", F = "", G = "", value = "", rem = "", stringsAsFactors = FALSE)
        downDATA <- rbind( downDATA, data)
      }
      
      colnames(downDATA) <- paste0("    ", colnames(LASTDATAVALUE), "    ")
      colnames(downDATA)[12] <- paste0("            ", colnames(downDATA)[12], "            ")
      tg = gridExtra::tableGrob(downDATA, rows = NULL )
      h = grid::convertHeight(sum(tg$heights), "in", TRUE)
      
      #Save table to pdf 2
      ggplot2::ggsave(paste0(con, ".2.pdf"), tg, width=20, height = h)
      
      pdf_combine(c(paste0(con, ".1.pdf"), paste0(con, ".2.pdf")), output = con)
      
    }
  }
)

# ================================================================================================================

#' Event when download CSV button is push
# output$downloadData <- downloadHandler(
#   filename = function() {
#     paste(getFullPopName(input$pointes), '.csv', sep='')
#   },
#   content = function(con) {
#     
#     allPosition <- getAllPosition()
#     allDate <- getAllDate()
#     allRem <- paste0("REM", 2:length(allDate))
#     text <- rep("", nrow(allPosition))
#     
#     line <- ""
#     for(i in 1:nrow(allPosition)) {
#       dataP <- allPosition[i, ]
#       
#       dataS <- getAllValueWithPID(dataP$PID)
#       
#       line <- paste0(dataP$PID, ";", dataP$POP, ";", dataP$Q, ";", dataP$A, ";", dataP$B, ";", dataP$C, ";", dataP$D, ";", dataP$E, ";", dataP$F, ";", dataP$G)
#       
#       indexD <- 1
#       for(date in allDate) {
#         if(indexD <= nrow(dataS)) {
#           dd <- dataS[indexD, ]
#           if(date == dd$date) {
#             indexD <- indexD + 1
#             line <- paste0(line, ";", dd$value, ";", dd$rem)
#           }
#           else {
#             line <- paste0(line, ";", 0, ";", 0)
#            }
#         }
#         else {
#           line <- paste0(line, ";", 0, ";", 0)
#         }
#       
#       }
#       
#       text[i] <- line
#      
#     }
#     
#     writeLines(text, con, sep = "\n")
#   }
# )
 
# ================================================================================================================

#' Event when quadrat is update
#observe({
#  
#  pointe <- isolate(input$pointes)
#  quadrat <- input$quadrat
#  
#  if(quadrat != "") {
#  
#      dates <- getDateByQuadratPop(pointe, quadrat)
#      
#      updateSelectInput(session, "dateAnalyse",
#                        label = "Date :",
#                        choices = dates,
#                        selected = dates[1]
#      )
#  }
#})
 
# ================================================================================================================

#' Event when push button 'Add new plant'
observeEvent(input$addNewPlant, {
  
  #Create default new plant slot
  pid <- max(LASTDATAVALUE$PID)+1
  DA <- 0
  DB <- 0
  DC <- 0
  DD <- 0
  DE <- 0
  DF <- 0
  DG <- 0
  value <- 0.5
  rem <- "0"
  
  if(!is.null(LASTDATAVALUE)) {
    
    pop <- LASTDATAVALUE$POP[1]
    q <- LASTDATAVALUE$Q[1]
    
    data <- data.frame(PID = pid, POP = pop, Q = q, A = DA, B = DB, C = DC, D = DD, E = DE, F = DF, G = DG, value = value, rem = rem, stringsAsFactors = FALSE)
    
    LASTDATAVALUE <<- rbind( LASTDATAVALUE, data)
    
    #Update table analyse
    output$tableAnalyse = DT::renderDataTable({
      DT::datatable(LASTDATAVALUE, editable = TRUE)
    })
  }
  
})
 
 # ================================================================================================================

#' Event when push button 'Add new plant 2'
observeEvent(input$addNewPlant2, {
  
  pid <- max(LASTDATAVALUE2$PID)+1
  DA <- 0
  DB <- 0
  DC <- 0
  DD <- 0
  DE <- 0
  DF <- 0
  DG <- 0
  value <- 0.5
  rem <- "0"
  
  if(!is.null(LASTDATAVALUE2)) {
    
    pop <- LASTDATAVALUE2$POP[1]
    q <- LASTDATAVALUE2$Q[1]
    
    data <- data.frame(PID = pid, POP = pop, Q = q, A = DA, B = DB, C = DC, D = DD, E = DE, F = DF, G = DG, value = value, rem = rem, stringsAsFactors = FALSE)
    
    LASTDATAVALUE2 <<- rbind( LASTDATAVALUE2, data)
    
    #Update table analyse 2
    output$tableAnalyse2 = DT::renderDataTable({
      DT::datatable(LASTDATAVALUE2, editable = TRUE)
    })
  }
  
})
 
 # ================================================================================================================ 

#' Event when push button 'Add new month'
observeEvent(input$addNewMonth, {

  date <- isolate(input$dateNew)
  
  for (index in 1:nrow(LASTDATAVALUE)) {
    data <- LASTDATAVALUE[index,]
    
    if(data$PID %in% FIRSTDATAVALUE$PID) {
      id <- setPosition(data$PID, data$POP, data$Q, data$A, data$B, data$C, data$D, data$E, data$F, data$G)
      
      if((FIRSTDATAVALUE %>% filter(PID == data$PID))$value == 0) {
        # nothing to do, is already dead
      } 
      else {
        addState(id, date, data$value, data$rem)
      }
    }
    else {
      id <- addPosition(data$PID, data$POP, data$Q, data$A, data$B, data$C, data$D, data$E, data$F, data$G)
      addState(id, date, data$value, data$rem)
    }
  }

})
 
# ================================================================================================================ 

#' Event when push button 'Add new pop'
observeEvent(input$addNewMonth2, {
  
  date <- isolate(input$dateNew2)
  
  for (index in 1:nrow(LASTDATAVALUE2)) {
    data <- LASTDATAVALUE2[index,]
    id <- addPosition(data$PID, data$POP, data$Q, data$A, data$B, data$C, data$D, data$E, data$F, data$G)
    addState(id, date, data$value, data$rem)
  }
  
})
 
# ================================================================================================================ 

#' Event when table Analyse cell is edit
observeEvent(input$tableAnalyse_cell_edit, {

 info <- input$tableAnalyse_cell_edit

 if(!is.null(LASTDATAVALUE)) {
   
      if(info$col == 12 || info$col == 2) {
        value <- info$value
      }
      else {
        value <- as.numeric(info$value)
        if(is.na(value)) {
          value <- 0
        }
      }
    
     LASTDATAVALUE[info$row, info$col] <- value
     
     LASTDATAVALUE <<- LASTDATAVALUE
     
 }

})
 
 
# ================================================================================================================ 

#' Event for render table analyse 2 
output$tableAnalyse2 = DT::renderDataTable({
  dataNewAnalyse <- data.frame(PID = 1, POP = "pop", Q = 0, A = 0, B = 0, C = 0, D = 0, E = 0, F = 0, G = 0, value = 0.5, rem = "0", stringsAsFactors = FALSE)
  LASTDATAVALUE2 <<- dataNewAnalyse
  DT::datatable(LASTDATAVALUE2, editable = TRUE)
})

# ================================================================================================================ 


#' Event when table Analyse cell is edit
observeEvent(input$tableAnalyse2_cell_edit, {
  
  info <- input$tableAnalyse2_cell_edit
  
  if(!is.null(LASTDATAVALUE2)) {
    
    if(info$col == 12 || info$col == 2) {
      value <- info$value
    }
    else {
      value <- as.numeric(info$value)
      if(is.na(value)) {
        value <- 0
      }
    }
    
    LASTDATAVALUE2[info$row, info$col] <- value
    
    LASTDATAVALUE2 <<- LASTDATAVALUE2
    
  }
  
})

# ================================================================================================================ 

#' Event when push button 'Draw plot'
observeEvent(input$drawAnalyse, {
  
  pointe <- isolate(input$pointes)

  quadrat <- isolate(input$quadrat)
  
  #date <- isolate(input$dateAnalyse)
  
  indexQ <- which(nbquad %in% quadrat)
  
  leg <- c("A", "B", "C", "D", "E", "F", "G")

  cx <- xx[indexQ, ]
  cy <- yy[indexQ, ]
  
  output$plotAnalyse1 <- renderPlot({
    
    p <- ggplot()
    
    
    pointQuadrat <- data.frame(x = numeric(), y = numeric(), name = character())
    pointQuadratGG <- data.frame(x = numeric(), y = numeric(), name = character())
    
    for(i in 1:7) {
      x <- cx[i]
      y <- cy[i]
      if(!is.na(x) && !is.na(y)) {
        pointQuadrat <- rbind( pointQuadrat, data.frame(x = (4*x), y = ((-1)*y), name =leg[i]))
        pointQuadratGG <- rbind( pointQuadratGG, data.frame(x = x, y = y, name =leg[i]))
      }
    }
    
    # Add quadrat points to ggplot
    p <- p + geom_point(data = pointQuadratGG, mapping = aes(x = x, y = y), size=3) + geom_text(data=pointQuadratGG,aes(x=x,y=y,label=name,size=2),hjust=-1, vjust=-1)
    
    jsonPointQuadrat <- toJSON(pointQuadrat)
    
    # Send quadrat points to javascript plot
    session$sendCustomMessage(type = 'drawPointQuadrat', message = jsonPointQuadrat)
    
    mkx <- c()
    mky <- c()
    mkxend <- c()
    mkyend <- c()
    range <- c()
    
    # For the construction of the quarat segment
    if(!(quadrat %in% casparticulier)) {
      range <- c(1, 2, 3, 4, 1)
    } 
    else {
      if(quadrat == 3) {
        range <- c(1, 2, 3, 4, 5, 6, 1)
      }
      else if(quadrat == 4) {
        range <- c(1, 2, 3, 4, 5, 1)
      }
      else if(quadrat == 17) {
        range <- c(1, 2, 3, 1)
      }
      else if(quadrat == 27) {
        range <- c(1, 2, 3, 1)
      }
      else if(quadrat == 38) {
        range <- c(1, 2, 3, 1)
      }
      else if(quadrat == 39) {
        range <- c(1, 2, 3, 4, 1)
      }
    }
    
    for(index in 1:(length(range)-1)) {
      i <- range[index]
      ax <- cx[i]
      ay <- cy[i]
      mkx <- c(mkx, ax)
      mky <- c(mky, ay)
      
    } 
    
    for(index in 2:(length(range))) {
      i <- range[index]
      ax <- cx[i]
      ay <- cy[i]
      mkxend <- c(mkxend, ax)
      mkyend <- c(mkyend, ay)
      
    } 
    
   # Add quadrat segements to ggplot  
   p <- p + geom_segment(aes(x = mkx, y = mky, xend = mkxend, yend = mkyend))
    
    if(quadrat == 21 || quadrat == 22) {
      # p <- layout(p, title = ' ',
      #              shapes = list(
      #                list(type = 'circle',
      #                     xref = 'x', x0 = -8, x1 = 8,
      #                     yref = 'y', y0 = -8, y1 = 8,
      #                     fillcolor = 'rgb(0, 0, 0)', line = list(color = 'rgb(0, 0, 0)'),
      #                     opacity = 0.1)))
    }
    
    newPoint <- input$newPointsRedraw
    
    # Get the plants value for the pop, quadrat & date selected
    dataValue <- getValue(pointe, quadrat, date)
    
    size <- nrow(dataValue)
    
    #' Update datetable raw data analyse
    output$tableAnalyse = DT::renderDataTable({
      LASTDATAVALUE <<- dataValue
      FIRSTDATAVALUE <<- dataValue
      DT::datatable(LASTDATAVALUE, editable = TRUE)
    })
    
    if(size > 0) {
      
      distance <- quad[quad$quadrat==quadrat,-c(1:2)]
      
      points <- data.frame(x = numeric(), y = numeric(), name = character(), value = numeric())
      pointsGG <- data.frame(x = numeric(), y = numeric(), name = character(), value = numeric(), alive=character())
      
      
      # Exeption for the quadra 27
      if(quadrat == 27) {
        ligne1=cbind(distance[1,1],distance[1,2],t(dataValue[,6]))
        ligne2=cbind(distance[1,2],distance[1,1],t(dataValue[,7]))
        point=rbind(ligne1,ligne2)
        
        ligne1=calcX(point)
        ligne2=-calcY(point,ligne1)
        if (quadrat==38) {ligne2=-ligne2} 
        A=ligne1[-(1:2)]
        B=ligne2[-(1:2)]
        
        
        for(i in 1:size) {
          plant <- dataValue[i,]
          
          sizeP <- 8
          rgb <- 'rgba(39, 174, 96, 1)' # green
          
          if(plant$value == 0) {
            sizeP <- 8
            rgb <- 'rgba(192, 57, 43, 1)' # red
          }
          
          if(!is.null(newPoint)) {
            
            if(length(newPoint)/4 == size) {
              
              indexJ <- i * 4 - 3
              
              x <- newPoint[indexJ]
              y <- newPoint[indexJ+1]
              name <- as.numeric(newPoint[indexJ+3])
              
              if(name == as.numeric(plant$PID)) {
                A[i] <- as.numeric(x)
                B[i] <- as.numeric(y)
              }
            }
          }
          
          points <- rbind( points, data.frame(x = (4*A[i]), y = ((-1)*B[i]), name =plant$PID, value = plant$value))
          
          alive <- 0
          
          if(plant$value > 0) {
            alive <- 1
          }
          
          pointsGG <- rbind( pointsGG, data.frame(x = (A[i]), y = (B[i]), name =plant$PID, value = plant$value, alive=alive))
          
        }
        
      }
      else {
        ligne1=cbind(distance[1,1],distance[1,2],t(dataValue[,4]))
        ligne2=cbind(distance[1,2],distance[1,1],t(dataValue[,5]))
        point=rbind(ligne1,ligne2)
        ligne1=calcX(point)
        ligne2=-calcY(point,ligne1)
        if (quadrat==38) {ligne2=-ligne2} 
        A=ligne1[-(1:2)]
        B=ligne2[-(1:2)]
        
        for(i in 1:size) {
          
          plant <- dataValue[i,]
          
          sizeP <- 8
          rgb <- 'rgba(39, 174, 96, 1)' # green
          
          if(plant$value == 0) {
            sizeP <- 8
            rgb <- 'rgba(192, 57, 43, 1)' # red
          }
          
          
          if(!is.null(newPoint)) {
            
            if(length(newPoint)/4 == size) {
              
              indexJ <- i * 4 - 3
              
              x <- newPoint[indexJ]
              y <- newPoint[indexJ+1]
              name <- as.numeric(newPoint[indexJ+3])
              
              if(name == as.numeric(plant$PID)) {
                A[i] <- as.numeric(x)
                B[i] <- as.numeric(y)
              }
            }
          }
          
          
          
          points <- rbind( points, data.frame(x = (4*A[i]), y = ((-1)*B[i]), name =plant$PID, value = plant$value))
          
          alive <- 0
          
          if(plant$value > 0) {
            alive <- 1
          }
          
          pointsGG <- rbind( pointsGG, data.frame(x = (A[i]), y = (B[i]), name =plant$PID, value = plant$value, alive=alive))
        
        }
      }
      
      # Add plant points to ggplot
      p <- p + geom_point(data = pointsGG, mapping = aes(x = x, y = y, color=cut(alive, c(-Inf, 0, 1, Inf))), size=2) + geom_text(data=pointsGG,aes(x=x,y=y,label=name,size=2),hjust=0, vjust=-0.5) + scale_color_manual(values = c("(-Inf,0]" = "red", "(0,1]" = "green", "(1, Inf]" = "green"))
      
      # Send plant points to javascript plot
      session$sendCustomMessage(type = 'drawPoint', message = points)
      
    }
    
    p <- p + ggtitle(POINTES_TITLE[as.numeric(quadrat)]) + theme_void() + theme(plot.title = element_text(hjust = 0.5, vjust=2), legend.position = "none")

    LAST_GGPLOT <<- p
    
    p
    
  })
  

  session$sendCustomMessage(type = 'cleanPlot', message = "")
  
})