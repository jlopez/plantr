Enferrets 1 - Quadrat 1 AB=190; BC=328; CD=170; AD=300
Enferrets 1 - Quadrat 2 AB=81; BC=110; CD=90; DA=81
Auzils - Quadrat 3 AB=120; BC=130; CD=285; DE=93; EF=280; AF=130
Auzils - Quadrat 4 AB=242; BC=290; CD=322; DE=363; EA=320
Auzils - Quadrat 5 AB=145; BC=146; CD=106; AD= 155
Peyrals - Quadrat 6 AB=109; BC=188; CD=147; AD=216,5 - Quadrat=Rocher
Peyrals - Quadrat 7 AB=248; BC=157; CD=108; DA=213
Peyrals - Quadrat 8 CD=108 - C=D de Q7 et D=C de Q7 / Quadrat= Rocher, Q7 au dessus
Peyrals - Quadrat 9 AB=224 - Quadrat = Rocher, pointes sur les pins
Peyrals - Quadrat 10 AB=66; BC=192; CD=101; AD=98
Enferrets 1 - Quadrat 11 AB=134; BC=230; CD=135; AD=226
Peyrals - Quadrat 12 4 faces du rocher pas de pointe
Enferrets 1 -Quadrat 13 AB=99; BC=100; CD=100; AD=97
Enferrets 1 -Quadrat 14 AB=155; BC=167+86; CD=186; AD=185+72
Enferrets 1 - Quadrat 15 AB=75; BC=113; CD=105; AD=100 (D=E12)
Enferrets 1 - Quadrat 16 AB=140; BC=58; CD=120; AD=79
Auzils - Quadrat 17 AB=174; BC=163; CA=167
Auzils - Quadrat 18 AB=BC=CD=AD=100
Auzils - Quadrat 19 AB=140; BC=43; CD=155; DA=134
Peyrals - Quadrat 20 AB=140; BC=178; CA=153
Cruzade - Quadrat 21 - Etiquette 45 - Rayon de 2 m autour
Cruzade - Quadrat 22 - Etiquette 46 - Rayon de 2 m autour
Peyrals - Quadrat 23 AB=160; BC=225; CD=113; DA=246 (C=pointe D de Q45)
Enferrets 1 - Quadrat 24 AB=66; BC=94; CD=138;DA=88
Enferrets 1 - Quadrat 25 AB=48; BC=145; CD=122;DA=98
Portes - Quadrat 26 AB=118; BC=82; CD=92; DA=45 (A=E58)
Portes - Quadrat 27 DC=92; CE=93; ED=80 (E=E59)
Enferrets 2 - Quadrat 28 AB=64; BC=70; CD=81; DA=74 (B=E28)
Enferrets 2 - Quadrat 29 AB=80; BC=110; CD= 102; DA=90 (B=E85)
Enferrets 2 - Quadrat 30 AB=49; BC=59; CD=95; DA= 91 (A=E88)
Enferrets 2 - Quadrat 31 AB=126; BC=298; CD=43; DA=82
Enferrets 2 - Quadrat 32 AB=74; BC=114; CA=115; d(B,E89)=84; d(C,E89)=52.5
Enferrets 2 - Quadrat 33 AB=91; BC=80; CD=123; DA=78; D=E100
Enferrets 2 - Quadrat 34 AB=75; BC=108; CA=91 -(B=E99)/Rocher, non limité par les pointes
Quadrat 35
Quadrat 36
Cruzade - Quadrat 37 AB=72 - Point A = Double Clou - B_ E410=43; A_E410=100
Cruzade - Quadrat 38 AB=147; AC=65; BC=128 - Pointe A = E166
Portes - Quadrat 39 AB=77;AC=119;AD= 100; BC=54; BD=127;CD=122
Enferrets 2 - Quadrat 40 AB=67; BC=147; CD=322; DA=240
Enferrets 1 - Quadrat 41 AB=101,5; BC=104; CD=75; DA=56
Enferrets 1 - Quadrat 42 AB=79,2; BC=52,2; CD=47; DA=43,5
Enferrets 1 - Quadrat 43 AB=100; BC=102; CD=106; DA=73 (B=pointe A de Q44)
Enferrets 1 - Quadrat 44 AB=48; BC=45; CD=67; DA=48,5 (A=pointe B de Q43)
Peyrals - Quadrat 45 AB=148; BC=202; CD=165; DA=131 (D=pointe C de Q23; B=E321)
